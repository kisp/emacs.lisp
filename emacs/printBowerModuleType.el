(defun printBowerModuleType (bowerJsonFileName)
  "prints out interesting info from bower.json"
  (princ (format "%s " bowerJsonFileName))
  (let ((json (json-read-file bowerJsonFileName)))
    (while json
      (setq val (pop json))
      (if (equal (car val) 'moduleType)
	  (princ (format "%s " (cdr val)))
      ))
    (princ "\n")
    ))
(provide 'printBowerModuleType)


;; (mapc 'printBowerModuleType
;;  (find-lisp-find-files "c:/dev/projects/mobile-dev-karma/" "bower.json")
;;  )
