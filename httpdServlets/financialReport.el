(defun showFinancialReportOnHttp ()
  "by calling this function it will put in the fin httpServlet to show the report"
  (interactive)
  (defun httpd/fin (proc path &rest args)
    (with-httpd-buffer proc "text/plain"
    (progn
	(require 'findFirstCSV)
	(require 'calculateOrderedGroups)
	(setq fileName (findFirstCSV))
	(princ (format "%s:\n" fileName))
	(calculateOrderedGroups fileName)
    )
    )))
(provide 'showFinancialReportOnHttp)
