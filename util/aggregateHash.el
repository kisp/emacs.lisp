(defun aggregateHash (hashMap key &optional numberValue)
  "summarize numberValues for the same key in a hashTable
this function returns the aggregated total value for the key

Example:
=======

(setq h (make-hash-table :test 'equal))
(aggregateHash h 'a 1) (aggregateHash h 'a 2) 
(aggregateHash h 'a) => 3

"
  (let (val)
      (setq val (gethash key hashMap))
      (if numberValue
	  (puthash key (if val (+ val numberValue) numberValue) hashMap)
	val)))
;; (aggregateHash myHash "b" 1231)
;; (aggregateHash myHash "b")
(provide 'aggregateHash)
