

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; package setup
(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))

;; load all of my lisp files
(cond
 ((eq "windows-nt" system-type) (add-to-list 'load-path "d:/DEV/.emacs.d/lisp/"))
 (t
  (add-to-list 'load-path "~/.emacs.d/lisp/emacs")
  (add-to-list 'auto-mode-alist '("\\.el\\.test$" . lisp-interaction-mode))
  (setq inferior-lisp-program "/usr/local/bin/clisp")
 ))
(setq user-emacs-directory "~/.emacs.d/lisp/emacs")
(progn
  (require 'loadAllLispFiles)
  (loadAllLispFiles))

;; display setup
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; erc setup
(setq erc-hide-list '("JOIN" "PART" "QUIT"))

;; org mode setup
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-isswitchdb)
;(setq org-log-done t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (deeper-blue)))
 '(package-selected-packages
   (quote
    (common-lisp-snippets elisp-format erefactor highlight-blocks highlight-defined highlight-escape-sequences highlight-indentation highlight-numbers highlight-parentheses highlight-quoted highlight-symbol lispyscript-mode log4e packed parinfer test-simple ttrss egg simple-httpd yaml-mode sos ac-slime elisp-slime-nav slime slime-volleyball xkcd json-mode magit)))
 '(semantic-mode t)
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
