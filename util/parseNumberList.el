(defun parseNumberList (string)
  "returns a list of numbers from a string"
  (interactive "sstring:")
  (require 'matchingRegexpList)
  (mapcar 'string-to-number (matchingRegexpList "[-\.0-9]+" string)))
(provide 'parseNumberList)
