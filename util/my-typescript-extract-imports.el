(defun my-typescript-extract-imports (file-path)
  "Extract import statements from a TypeScript file and build a directed graph using DOT language."
  (interactive "fEnter TypeScript file path: ")
  (let ((visited-files (make-hash-table :test 'equal))
        (dot-graph ""))
    (my-typescript-extract-imports-recursive file-path visited-files dot-graph)
    (my-save-dot-file dot-graph)))
(provide 'my-typescript-extract-imports)

(defun my-typescript-extract-imports-recursive (file-path visited-files dot-graph)
  "Helper function to extract import statements recursively and build the DOT graph."
  (unless (gethash file-path visited-files)
    (puthash file-path t visited-files)
    (with-temp-buffer
      (insert-file-contents file-path)
      (goto-char (point-min))
      (let (imports import-regexp import-statement imported-file)
        (setq import-regexp "import\\s-+{\\(.*?\\)}\\s-+from\\s-+\"\\(.*?\\)\";")
        (while (re-search-forward import-regexp nil t)
          (setq imports (match-string 1))
          (setq import-statement (match-string 0))
          (setq imported-file (expand-file-name (match-string 2) (file-name-directory file-path)))
          (message "Imports in %s: %s" file-path imports)
          ;; You can use the `import-statement` variable here for further processing

          ;; Add the file and its imported file to the DOT graph
          (setq dot-graph (format "%s\"%s\" -> \"%s\";\n" dot-graph file-path imported-file))

          ;; Extract imports from the imported file recursively
          (when (file-exists-p imported-file)
            (my-typescript-extract-imports-recursive imported-file visited-files dot-graph)))))))

(defun my-save-dot-file (dot-graph)
  "Save the DOT graph to a file."
  (interactive)
  (let* ((default-directory "~/") ; Change the directory as per your preference
         (dot-file-path (expand-file-name "typescript-graph.dot" default-directory)))
    (with-temp-file dot-file-path
      (insert dot-graph))
    (message "DOT file saved: %s" dot-file-path)))

