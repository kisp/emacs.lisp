(defun findFirstCSV ()
  "returns the first processable CSV file name"
  (elt
   (find-lisp-find-files
    (cond ((string-equal "windows-nt" system-type) "f:/financial")
	  (t "~/Dropbox/fontos/financial"))
    "hitel.*utf8\\.out\\.csv$")
   0))
(provide 'findFirstCSV)
