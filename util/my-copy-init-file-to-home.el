(defun my-copy-init-file-to-home ()
  "Copy the init.el file to the user's home directory."
  (interactive)
  (let* ((init-file (expand-file-name "init.el" user-emacs-directory))
         (home-dir (expand-file-name "~/")))
    (copy-file init-file home-dir t)
    (message "init.el copied to home directory.")))
(provide 'my-copy-init-file-to-home)
