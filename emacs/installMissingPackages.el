(defun installMissingPackages (installed required)
"
(setq installedPackages
      (quote
       (common-lisp-snippets dired-k egg svg-clock mocha npm-mode tide yaml-mode simple-httpd json-mode magit)))


(setq requiredPackages
      (quote
       (common-lisp-snippets elisp-format erefactor highlight-blocks highlight-defined highlight-escape-sequences highlight-indentation highlight-numbers highlight-parentheses highlight-quoted highlight-symbol lispyscript-mode log4e packed parinfer test-simple ttrss egg simple-httpd yaml-mode sos ac-slime elisp-slime-nav slime slime-volleyball xkcd json-mode magit)))
(installMissingPackages installedPackages requiredPackages)
"
  (let (result)
  (dolist (package required result)
    (unless (member installed package)
      (package-install package)
      (push package result)))))

(provide 'installMissingPackages)

