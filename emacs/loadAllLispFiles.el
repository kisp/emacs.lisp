(defun loadAllLispFiles ()
  "loads all my lisp files, except init.el to avoid recursion"
  (interactive)
  (require 'myLispFiles)
  (dolist (file (myLispFiles) )
    (cond
     ((string-match-p "init\.el$" file))
     (t (load file)))))
(provide 'loadAllLispFiles)
