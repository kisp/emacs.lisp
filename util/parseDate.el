(defun parseDate (dateStr)
  "Parses the date from string \"YYYY.mm.DD\" to a list of numbers
  Example:
  (parseDate \"2016.03.12\") -> (2015 3 12)"
  (interactive "sdateStr:")
  (string-match "^\\([0-9]+\\)\.\\([0-9]+\\)\.\\([0-9]+\\).*" dateStr)
  (mapcar 'string-to-number (list
			     (match-string 1 dateStr)
			     (match-string 2 dateStr)
			     (match-string 3 dateStr))))

(provide 'parseDate)
