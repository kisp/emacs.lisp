My scripts
==========

All the .el files in this directory get loaded on startup.
This tend to be my utility and safety belt...
:)

What?
-----

Helper functions for everyday life.

How?
----

Parsing and processing data as lists.

Clone this repo to `.emacs.d/lisp` folder.
Copy `init.el` to home.

Why?
----

My goal with this is to understand better what's going on around.
