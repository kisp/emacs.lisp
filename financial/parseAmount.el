(defun parseAmount (inputStr)
  "sanitize amount string and parse as a number -123_456,789 EUR -> -123456.789"
  (interactive "sinputStr:")
    (let ((invalid "[^0-9\.,]"))
      (string-to-number
       (replace-regexp-in-string "," "."
				(replace-regexp-in-string invalid "" inputStr)))
      ))
(provide 'parseAmount)
