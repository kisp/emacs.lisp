(defun my-git-checkout (repo-url destination)
  "Checkout a Git repository from GitHub."
  (interactive "sEnter GitHub repository URL: \nD")
  (let* ((default-directory (file-name-as-directory destination))
         (repo-name (file-name-nondirectory (directory-file-name repo-url))))
    (message "Cloning %s..." repo-url)
    (shell-command (format "git clone %s %s" repo-url repo-name))
    (message "Cloning completed!")))
(provide 'my-git-checkout)
