(defun testType (&rest variable)
  "This is my first elisp test."
  (interactive "svariableName: ")
  (cond
   ((stringp variable) (princ (format "string : %s\n" variable)))
   ((numberp variable) (princ (format "number : %d\n" variable)))
   ((listp variable) (princ (format "list : %S\n" variable)))
   (t (princ "Input type not detected!\n"))))

(provide 'testType)
