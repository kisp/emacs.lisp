(defun servletStart ()
  "This function fires up a http-server on the default port
defined by 'httpd-port with the use of 'simple-httpd package."
  (interactive)
  (package-activate 'simple-httpd)

  (require 'simple-httpd)
  
  (message "Starting server on port %d..." httpd-port)
  (httpd-start)
)

(provide 'servletStart)
