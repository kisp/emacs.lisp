(defun httpd/ (proc path &rest args)
  "It defines a basic servlet to provide information about the request.
By default there is a file servlet, which is overridden with this root context servlet.
For more information see the help of the 'simple-httpd package"
  (with-httpd-buffer proc "text/plain"
    (progn
      (insert (format "path %s\nargs %S" path args))
      )))
