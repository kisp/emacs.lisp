(defun my-get-weather (city)
  "Get the current weather conditions for a given city using OpenWeatherMap API.

This API is not working anymore
"
  (interactive "sEnter city name: ")
  (let* ((api-key "YOUR_API_KEY") ; Replace with your OpenWeatherMap API key
         (api-url (format "http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s"
                          city api-key))
         (json-object-type 'plist)
         (weather-data (json-read-from-string (url-retrieve-synchronously api-url))))
    (when (plist-get weather-data :cod)
      (let* ((temperature (plist-get-in weather-data '(:main :temp)))
             (weather-description (plist-get-in weather-data '(:weather 0 :description))))
        (message "Current weather in %s: Temperature: %.1f°C, Description: %s"
                 city (kelvin-to-celsius temperature) weather-description)))))(defun my-get-weather (city)
  "Get the current weather conditions for a given city using MetaWeather API."
  (interactive "sEnter city name: ")
  (let* ((api-url (format "https://www.metaweather.com/api/location/search/?query=%s"
                          (url-hexify-string city)))
         (json-object-type 'plist)
         (location-data (json-read-from-string (url-retrieve-synchronously api-url))))
    (when (and location-data (plist-get location-data 0))
      (let* ((woeid (plist-get (plist-get location-data 0) :woeid))
             (weather-api-url (format "https://www.metaweather.com/api/location/%s/" woeid))
             (weather-data (json-read-from-string (url-retrieve-synchronously weather-api-url))))
        (when (plist-get weather-data :consolidated_weather)
          (let* ((consolidated-weather (plist-get weather-data :consolidated_weather))
                 (current-weather (plist-get (car consolidated-weather) :weather_state_name))
                 (current-temp (plist-get (car consolidated-weather) :the_temp)))
            (message "Current weather in %s: Temperature: %.1f°C, Weather: %s"
                     city current-temp current-weather)))))))
(provide 'my-get-weather)
