(defun loadFinancialData (fileName)
  "This function is intented to load my finantial report CSV into a hash Map
aggregated by subjects, in the future I will make some simplifications"
  (let (lines currentLine myHash subject amount (sum 0) date summary)
    (require 'parseDate)
    (require 'parseAmount)
    (require 'aggregateHash)
    (require 'financialGrouping)
    (require 'readTextFile)
    (setq summary (make-hash-table :test 'equal))

    ;; TODO extract this
    (setq lines (readTextFile fileName))
    (while (and
	    (> (length lines) 0)
	    (not (string-prefix-p "Könyvelt tételek" (pop lines)))))
    (pop lines)

    (setq myHash (make-hash-table :test 'equal))

    ;; TODO make this parametrized!
    (while lines
      (setq currentLine (split-string (pop lines) ";"))
      ;; (setq date (parseDate (elt currentLine 1)))
      (setq subject (financialGrouping ; group by column 6
		     (elt currentLine 6)))
      (when (= 0 (length subject)) ; if no subject, pick it from the first column
	(setq subject (nth 0 currentLine)))

      (setq amount (parseAmount (elt currentLine 4)))
      (setq sum (+ sum amount))
      ;;(message "%s : %d  from %S" subject amount currentLine)

      (aggregateHash summary subject amount)
      )
    
    (message "Total Summary : %d" sum)
    ;;  (message "Use hash summary for queries")
    summary
    )
  )
(provide 'loadFinancialData)
