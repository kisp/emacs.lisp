(defun calculateOrderedGroups (fileName)
  "Aggregates financial subjects by predefined groups"
  (let (summary list sortedList e)
    (require 'loadFinancialData)
    (require 'subr-x)
    (setq summary (loadFinancialData fileName))
    (setq list (hash-table-keys summary))
    (setq sortedList (sort list
			   (lambda (a b)
			     (< (aggregateHash summary a)
				(aggregateHash summary b)))))

    (while sortedList
      (setq e (pop sortedList))
      (princ (format "%s : %d\n" e (aggregateHash summary e)))))
  )

(provide 'calculateOrderedGroups)
