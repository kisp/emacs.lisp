(defun summarizeTotal (string)
  "This function returns the summarized total amount in a string.
You can call this with shoping lists or notes. 
All the recognized numbers got summed up.
"
  (let ((sum 0))
    (dolist (e (parseNumbersList string) sum)
      (setq sum (+ sum e)))))
(provide 'summarizeTotal)
