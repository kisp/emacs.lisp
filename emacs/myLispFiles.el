(require 'find-lisp)
(defun myLispFiles ()
  "list all my elisp files in .emacs.d"
  (find-lisp-find-files "~/.emacs.d/lisp/" "^[^#]+\.el$"))
(provide 'myLispFiles)
