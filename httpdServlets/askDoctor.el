(defun askDoctor ()
  "defines a servlet to share the *doctor* buffer's output"
  (interactive)

  (defservlet doctor text/plain ()
    (insert-buffer-substring (get-buffer-create "*doctor*")))
  )
(provide 'askDoctor)
