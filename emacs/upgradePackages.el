(defun upgradePackages ()
    "This function opens the package list, and installs the updates"
  (progn (package-list-packages)
       (with-current-buffer "*Packages*"
	 (progn (package-menu-mark-upgrades)
		(package-install-selected-packages)))))
(provide 'upgradePackages)
